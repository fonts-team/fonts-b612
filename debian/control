Source: fonts-b612
Section: fonts
Priority: optional
Maintainer: Debian Fonts Task Force <debian-fonts@lists.debian.org>
Uploaders: Alex Myczko <tar@debian.org>
Build-Depends: debhelper-compat (= 13), fontmake
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/fonts-team/fonts-b612.git
Vcs-Browser: https://salsa.debian.org/fonts-team/fonts-b612
Homepage: https://github.com/polarsys/b612

Package: fonts-b612
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: legible font designed to be used on aircraft cockpit screens
 In 2010, Airbus initiated a research collaboration with ENAC and Universite
 de Toulouse III on a prospective study to define and validate an Aeronautical
 Font: the challenge was to improve the display of information on the cockpit
 screens, in particular in terms of legibility and comfort of reading, and to
 optimize the overall homogeneity of the cockpit.
 .
 Two years later, Airbus came to find Intactile DESIGN to work on the design
 of the eight typographic variants of the font. This one, baptized B612 in
 reference to the imaginary asteroid of the aviator SaintExupery, benefited
 from a complete hinting on all the characters.
 .
 Main characteristics are:
  - Maximize the distance between the forms of the characters
  - Respect the primitives of the different letters
  - Harmonize the forms and their spacing
